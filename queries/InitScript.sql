CREATE USER IF NOT EXISTS 'mysql_user'@'localhost' IDENTIFIED BY 'Password';
GRANT ALL PRIVILEGES ON *.* TO 'mysql_user'@'localhost';

CREATE DATABASE IF NOT EXISTS FSPoland;

USE FSPoland;

DROP TABLE IF EXISTS SkipadRuns;
DROP TABLE IF EXISTS Skipads;
DROP TABLE IF EXISTS AccelerationRuns;
DROP TABLE IF EXISTS Accelerations;
DROP TABLE IF EXISTS Endurance;
DROP TABLE IF EXISTS Endurance_laps;
DROP TABLE IF EXISTS Autocross;
DROP TABLE IF EXISTS AutocrossRuns;
DROP TABLE IF EXISTS DesignReport;
DROP TABLE IF EXISTS BusinessPlanPresentation;
DROP TABLE IF EXISTS CostReport;

DROP TABLE IF EXISTS Contestants;
DROP TABLE IF EXISTS Judges;
DROP TABLE IF EXISTS Teams;
DROP TABLE IF EXISTS Colleges;
DROP TABLE IF EXISTS MemberTypes;
DROP TABLE IF EXISTS JudgeTypes;

CREATE TABLE IF NOT EXISTS Colleges(
    CollegeID INT AUTO_INCREMENT,
    Name VARCHAR(50) NOT NULL,
    City VARCHAR(20) NOT NULL,
    Country VARCHAR(20) NOT NULL,

    PRIMARY KEY (CollegeID)
);

CREATE TABLE IF NOT EXISTS Teams(
    TeamID INT AUTO_INCREMENT,
    Name VARCHAR(50) NOT NULL,
    CollegeID INT NOT NULL,

    PRIMARY KEY (TeamID),
    FOREIGN KEY (CollegeID) REFERENCES Colleges(CollegeID)
);

CREATE TABLE IF NOT EXISTS MemberTypes(
    MemberTypeID INT AUTO_INCREMENT,
    MemberTypeName VARCHAR(25) NOT NULL,

    PRIMARY KEY (MemberTypeID)
);

CREATE TABLE IF NOT EXISTS JudgeTypes(
    JudgeTypeID INT AUTO_INCREMENT,
    JudgeTypeName VARCHAR(40) NOT NULL,

    PRIMARY KEY (JudgeTypeID)
);

CREATE TABLE IF NOT EXISTS Contestants(
    ContestantID INT AUTO_INCREMENT,
    FirstName VARCHAR(15) NOT NULL,
    LastName VARCHAR(40) NOT NULL,
    DateOfBirth DATE NOT NULL,
    TeamID INT NOT NULL,
    MemberType INT NOT NULL,
    Paid BOOL,
    Email VARCHAR(50) NOT NULL,
    PhoneNumber VARCHAR(15) NOT NULL,
    PhoneNumberEmergency VARCHAR(15) NOT NULL,

    PRIMARY KEY (ContestantID),
    FOREIGN KEY (MemberType) REFERENCES MemberTypes(MemberTypeID),
    FOREIGN KEY (TeamID) REFERENCES Teams(TeamID)
);

CREATE TABLE IF NOT EXISTS Judges(
    JudgeID INT AUTO_INCREMENT,
    FirstName VARCHAR(15) NOT NULL,
    LastName VARCHAR(40) NOT NULL,
    DateOfBirth Date NOT NULL,
    Email VARCHAR(50) NOT NULL,
    PhoneNumber VARCHAR(15) NOT NULL,
    JudgeType INT NOT NULL,

    PRIMARY KEY (JudgeID),
    FOREIGN KEY (JudgeType) REFERENCES JudgeTypes(JudgeTypeID)
);

CREATE TABLE IF NOT EXISTS SkipadRuns(
    SkipadRunID INT AUTO_INCREMENT,
    RunTime TIME,
    DriverID INT NOT NULL,
    RunNumber INT NOT NULL CHECK ( RunNumber >= 1 AND RunNumber <= 4 ),

    PRIMARY KEY (SkipadRunID),
    FOREIGN KEY (DriverID) REFERENCES Contestants(ContestantID)
);

CREATE TABLE IF NOT EXISTS Skipads(
    SkipadFinalRunID INT AUTO_INCREMENT,
    FinalTime TIME,
    TeamID INT NOT NULL,

    PRIMARY KEY (SkipadFinalRunID),
    FOREIGN KEY (TeamID) REFERENCES Teams(TeamID)
);

CREATE TABLE IF NOT EXISTS Accelerations(
    AccelerationRunID INT AUTO_INCREMENT,
    FinalTime TIME,
    TeamID INT NOT NULL,

    PRIMARY KEY (AccelerationRunID),
    FOREIGN KEY (TeamID) REFERENCES Teams(TeamID)
);

CREATE TABLE IF NOT EXISTS AccelerationRuns(
    AccelerationFinalRunID INT AUTO_INCREMENT,
    RunTime TIME,
    DriverID INT NOT NULL,
    TeamRunNumber INT NOT NULL CHECK ( TeamRunNumber >= 1 AND TeamRunNumber <= 4 ),

    PRIMARY KEY (AccelerationFinalRunID),
    FOREIGN KEY (DriverID) REFERENCES Contestants(ContestantID)
);

CREATE TABLE IF NOT EXISTS Autocross(
    AutocrossFinalRunID INT AUTO_INCREMENT,
    RunTime TIME,
    TeamID INT NOT NULL,

    PRIMARY KEY (AutocrossFinalRunID),
    FOREIGN KEY (TeamID) REFERENCES Teams(TeamID)
);

CREATE TABLE IF NOT EXISTS AutocrossRuns(
    AutocrossRunID INT AUTO_INCREMENT,
    RunTime TIME,
    DriverID INT NOT NULL,
    TeamRunNumber INT NOT NULL CHECK ( TeamRunNumber >= 1 AND TeamRunNumber <= 4 ),

    PRIMARY KEY (AutocrossRunID),
    FOREIGN KEY (DriverID) REFERENCES Contestants(ContestantID)
);

CREATE TABLE IF NOT EXISTS Endurance(
    EnduranceTeamID INT AUTO_INCREMENT,
    TeamID INT NOT NULL,
    BestTime TIME,
    LastTime TIME,
    Lap INT NOT NULL,

    PRIMARY KEY (EnduranceTeamID),
    FOREIGN KEY (TeamID) REFERENCES Teams(TeamID)
);

CREATE TABLE IF NOT EXISTS Endurance_laps(
    EnduranceLapID INT AUTO_INCREMENT,
    LapNum INT,
    LapTime TIME,
    DriverID INT NOT NULL,

    PRIMARY KEY (EnduranceLapID),
    FOREIGN KEY (DriverID) REFERENCES Contestants(ContestantID)
);

CREATE TABLE IF NOT EXISTS DesignReport(
    DesignReportID INT AUTO_INCREMENT,
    TeamID INT NOT NULL,
    Points INT,
    DateOfReport TIME NOT NULL,

    Judge1 INT NOT NULL,
    Judge2 INT NOT NULL,
    Judge3 INT NOT NULL,
    Judge4 INT NOT NULL,
    Judge5 INT NOT NULL,

    PRIMARY KEY (DesignReportID),
    FOREIGN KEY (TeamID) REFERENCES Teams(TeamID),
    FOREIGN KEY (Judge1) REFERENCES Judges(JudgeID),
    FOREIGN KEY (Judge2) REFERENCES Judges(JudgeID),
    FOREIGN KEY (Judge3) REFERENCES Judges(JudgeID),
    FOREIGN KEY (Judge4) REFERENCES Judges(JudgeID),
    FOREIGN KEY (Judge5) REFERENCES Judges(JudgeID)
);

CREATE TABLE IF NOT EXISTS BusinessPlanPresentation(
    BusinessPlanPresentation INT AUTO_INCREMENT,
    TeamID INT NOT NULL,
    Points INT,
    DateOfPresentation TIME NOT NULL,

    PRIMARY KEY (BusinessPlanPresentation),
    FOREIGN KEY (TeamID) REFERENCES Teams(TeamID)
);

CREATE TABLE IF NOT EXISTS CostReport(
    CostReportID INT AUTO_INCREMENT,
    TeamID INT NOT NULL,
    Points INT,
    DateOfReport TIME NOT NULL,

    Judge1 INT NOT NULL,
    Judge2 INT NOT NULL,
    Judge3 INT NOT NULL,

    PRIMARY KEY (CostReportID),
    FOREIGN KEY (TeamID) REFERENCES Teams(TeamID),
    FOREIGN KEY (Judge1) REFERENCES Judges(JudgeID),
    FOREIGN KEY (Judge2) REFERENCES Judges(JudgeID),
    FOREIGN KEY (Judge3) REFERENCES Judges(JudgeID)
);

INSERT MemberTypes(MemberTypeName)
VALUES ('Leader'),
       ('Member'),
       ('Driver'),
       ('Driver, Leader');

INSERT JudgeTypes(JudgeTypeName)
VALUES ('Technical Inspector'),
       ('Senior Technical Inspector'),
       ('Judge'),
       ('Special Judge');

INSERT Colleges(Name, City, Country)
VALUES ('Politechnika Poznanska', 'Poznan', 'Polska'),
       ('Politechnika Wroclawska', 'Wroclaw', 'Polska');

INSERT Teams(Name, CollegeID)
VALUES ('PUT Motorsport', 1),
       ('PUT eMotorsport', 1),
       ('PWR Racing Team', 2);

INSERT Contestants(FirstName, LastName, DateOfBirth, TeamID, MemberType, Paid, Email, PhoneNumber, PhoneNumberEmergency)
VALUES ('Piotr', 'Kwiatkowski', DATE('1998-01-15'), 2, 1, 1, 'kwiat1998P@wp.pl', '886153973', 'XXXXXXXXX'),
       ('Marcin', 'Marciniak', DATE('1990-01-15'), 1, 1, 1, 'XXXXXXXX@XXXX', 'XXXXXXXXX', 'XXXXXXXXX'),
       ('Jan', 'Ktos', DATE('1990-01-15'), 1, 2, 1, 'XXXXXXXX@XXXX', 'XXXXXXXXX', 'XXXXXXXXX'),
       ('Pawel', 'Ktos', DATE('1990-01-15'), 2, 1, 1, 'XXXXXXXX@XXXX', 'XXXXXXXXX', 'XXXXXXXXX'),
       ('Maciej', 'Ktos', DATE('1990-01-15'), 3, 1, 1, 'XXXXXXXX@XXXX', 'XXXXXXXXX', 'XXXXXXXXX'),
       ('Mateusz', 'Ktos', DATE('1990-01-15'), 3, 2, 1, 'XXXXXXXX@XXXX', 'XXXXXXXXX', 'XXXXXXXXX');

INSERT Judges(FirstName, LastName, DateOfBirth, Email, PhoneNumber, JudgeType)
VALUES ('Pedro', 'Pizdini', DATE('1990-01-15'), 'XXXXXX@XXXX', 'XXXXXXXXX', 1),
       ('Inny', 'Sloch', DATE('1990-01-15'), 'XXXXXX@XXXX', 'XXXXXXXXX', 2),
       ('Antonio', 'Tuttifrutti', DATE('1990-01-15'), 'XXXXXX@XXXX', 'XXXXXXXXX', 3),
       ('Enzo', 'Ferrari', DATE('1990-01-15'), 'XXXXXX@XXXX', 'XXXXXXXXX', 4);


