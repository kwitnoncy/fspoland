from flask import Flask, render_template, redirect, request, url_for, templating
import os
import json
from utility import *

app = Flask(__name__)


@app.route('/', methods=['GET'])
def hello_world():
    return redirect('/index')


@app.route('/index', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/teams', methods=['GET', 'PUT', 'DELETE'])
def teams() -> json or templating:
    if request.method == 'GET':
        query_file = open(os.getcwd() + '/queries/teams_list.sql', encoding='utf-8', mode='r')
        query = query_file.read(-1)
        data = execute(query)

        if request.content_type == 'application/json':
            to_send = []
            for row in data:
                x = {
                    'TeamID': row[0],
                    'TeamName': row[1],
                    'CollegeID': row[2],
                    'CollegeName': row[4],
                    'City': row[5],
                    'Country': row[6]
                }
                to_send.append(x)

            return json.dumps({"endpoint": "teams",
                               "type": "json",
                               "data": to_send})
        else:
            return render_template('teams.html', value=data)
    elif request.method == 'PUT':
        data = request.json
        team_name = data['TeamName']
        college_name = data['CollegeName']
        city_name = data['City']
        country_name = data['Country']

        query_file = open(os.getcwd() + '/queries/teams_list.sql', encoding='utf-8', mode='r')
        query = query_file.read(-1)
        teams = execute(query)

        for row in teams:
            if row[1] == team_name:
                return json.dumps({'endpoint': 'teams',
                                   'method': 'PUT',
                                   'status': 'Team has been already added',
                                   'data': [row]})

        query_file = open(os.getcwd() + '/queries/colleges_list.sql', encoding='utf-8', mode='r')
        query = query_file.read(-1)
        colleges = execute(query)
        college_id = None

        for row in colleges:
            if row[1] == college_name and row[2] == city_name and row[3] == country_name:
                college_id = row[0]

        if college_id is None:
            query_file = open(os.getcwd() + '/queries/insert_college.sql', encoding='utf-8', mode='r')
            execute_insert(query_file.read(-1), (college_name, city_name, country_name))

            query_file = open(os.getcwd() + '/queries/colleges_list.sql', encoding='utf-8', mode='r')
            colleges = execute(query_file.read(-1))

            for row in colleges:
                if row[1] == college_name and row[2] == city_name and row[3] == country_name:
                    college_id = row[0]

        query_file = open(os.getcwd() + '/queries/insert_team.sql', encoding='utf-8', mode='r')
        query = query_file.read(-1)
        execute_insert(query, (team_name, college_id))

        return json.dumps({'endpoint': 'teams',
                           'method': 'PUT',
                           'status': 'Added team'})
    elif request.method == 'DELETE':
        # usuniecie teamu
        pass


@app.route('/teams/<int:team_id>', methods=['GET'])
def team_by_id(team_id: int) -> json:
    query_file = open(os.getcwd() + '/queries/teams_list.sql', encoding='utf-8', mode='r')
    query = query_file.read(-1)
    data = execute(query)

    if request.content_type == 'application/json':
        to_send = []
        for row in data:
            if int(row[0]) == team_id:
                to_send = [{'TeamID': row[0],
                            'TeamName': row[1],
                            'CollegeID': row[2],
                            'CollegeName': row[4],
                            'City': row[5],
                            'Country': row[6]}]
                break

        return json.dumps({"endpoint": "teams",
                           "type": "json",
                           "data": to_send})
    else:
        to_send = []
        for row in data:
            if int(row[0]) == team_id:
                to_send = [row]
                break
        print(to_send)
        return render_template('teams.html', value=to_send)


@app.route('/contestants', methods=['GET', 'PUT', 'DELETE'])
def contestants() -> json or templating:
    if request.method == 'GET':
        query_file = open(os.getcwd() + '/queries/contestants_list.sql', encoding='utf-8', mode='r')
        query = query_file.read(-1)
        data = execute(query)

        if request.content_type == 'application/json':
            to_send = []
            for row in data:
                x = {
                    'ContestantsID': row[0],
                    'FirstName': row[1],
                    'LastName': row[2],
                    'DateOfBirth': str(row[3]),
                    'TeamName': row[4],
                    'MemberType': row[5]
                }
                to_send.append(x)

            return json.dumps({"endpoint": "teams",
                               "type": "json",
                               "data": to_send})
        else:
            return render_template('contestants.html', value=data)
    elif request.method == 'PUT':
        data = request.json
        try:
            first_name = data['FirstName']
            last_name = data['LastName']
            team_name = data['TeamName']
            member_type = data['MemberType']
            date_of_birth = data['DateOfBirth']
            paid = data['paid']
            email = data['email']
            phone_number = data['PhoneNumber']
            phone_number_emergency = data['PhoneNumberEmergency']
        except ValueError:
            return json.dumps({'endpoint': 'contestants',
                               'method': 'PUT',
                               'status': 'Wrong data: ValueError'})

        query_file = open(os.getcwd() + '/queries/teams_list.sql', encoding='utf-8', mode='r')
        teams = execute(query_file.read(-1))
        team_id = None

        for row in teams:
            if row[1] == team_name:
                team_id = row[0]
                break

        if team_id is None:
            return json.dumps({'endpoint': 'contestants',
                               'method': 'PUT',
                               'status': 'Wrong data: No such team'})

        # find MemberTypeID
        query_file = open(os.getcwd() + '/queries/membertypes_list.sql', encoding='utf-8', mode='r')
        types = execute(query_file.read(-1))
        member_type_id = None

        for row in types:
            if row[1] == member_type:
                member_type_id = row[0]
                print(row)
                break

        if member_type_id is None:
            return json.dumps({'endpoint': 'contestants',
                               'method': 'PUT',
                               'status': 'Wrong data: No such member type'})

        query_file = open(os.getcwd() + '/queries/insert_contestant.sql', encoding='utf-8', mode='r')
        execute_insert(query_file.read(-1), (first_name, last_name, date_of_birth, team_id, member_type_id, paid, email,
                                              phone_number, phone_number_emergency))

        return json.dumps({'endpoint': 'contestants',
                           'method': 'PUT',
                           'status': 'ok'})
    elif request.method == 'DELETE':
        pass


@app.route('/contestants/<string:team_name>', methods=['GET'])
def contestants_by_team_name(team_name: str) -> json or templating:
    query_file = open(os.getcwd() + '/queries/contestants_list.sql', encoding='utf-8', mode='r')
    query = query_file.read(-1)
    data = execute(query)

    if request.content_type == 'application/json':
        to_send = []
        for row in data:
            if row[4] == team_name:
                x = {
                    'ContestantsID': row[0],
                    'FirstName': row[1],
                    'LastName': row[2],
                    'DateOfBirth': str(row[3]),
                    'TeamName': row[4],
                    'MemberType': row[5]
                }
                to_send.append(x)

        return json.dumps({"endpoint": "teams",
                           "type": "json",
                           "data": to_send})
    else:
        to_send = []
        for row in data:
            if row[4] == team_name:
                to_send.append(row)

        return render_template('contestants.html', team_name=team_name, value=to_send)


@app.route('/judges')
def judges():
    return 'judges'


@app.route('/autocross')
def autocross():
    return 'autocross'


if __name__ == '__main__':
    app.run(host='0.0.0.0')
