import mysql.connector as mysql


def execute(query: str, parameters: tuple = None) -> list:
    connection = mysql.connect(host='localhost',
                               database='FSPoland',
                               user='mysql_user',
                               password='Password')
    cursor = connection.cursor()
    if parameters is None:
        cursor.execute(query)
    else:
        cursor.execute(query, parameters)

    return cursor.fetchall()


def execute_insert(query: str, parameters: tuple = None) -> None:
    connection = mysql.connect(host='localhost',
                               database='FSPoland',
                               user='mysql_user',
                               password='Password')
    cursor = connection.cursor()
    if parameters is None:
        cursor.execute(query)
    else:
        cursor.execute(query, parameters)

    connection.commit()
