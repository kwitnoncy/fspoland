FROM python:3.7-alpine
EXPOSE 5000
WORKDIR /app
COPY ./requirements.txt /app
COPY ./app.py /app
COPY ./queries /app
COPY ./templates /app
COPY ./utility.py /app
RUN pip install -r requirements.txt

CMD python app.py