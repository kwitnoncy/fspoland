# Formula Student Poland

## Overview

This is simple REST (or wannabe-REST) application that connects database and provide useful information to contestants 
and judges (like results, team's member list).

## Tools/technologies

1. Pycharm (to write application code):
    - Python(3.7, 64-bit)
    - Flask (1.0.3)
    - mysql
2. DataGrip (to check database):
    - SQL Server database
3. Postman (to test)
4. My own python client (also to test)

## Requirements
1. python 3+
2. pip 19+
3. flask
4. mysql.connection 
5. Internet connection (optional) 

## How to run
If your machine have not installed flask and mysql.connection yet:

```
sudo ./runInstall.sh
```

If you already have installed flask and mysql.connection run:

```
sudo ./run.sh
```

